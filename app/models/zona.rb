class Zona < ApplicationRecord

  belongs_to :municipio, class_name: 'Sip::Municipio',
    foreign_key: 'municipio_id', validate: true, optional: false

end


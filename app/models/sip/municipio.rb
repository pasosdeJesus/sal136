
require 'sip/concerns/models/municipio'

module Sip
  class Municipio < ActiveRecord::Base
    include Sip::Concerns::Models::Municipio

    scope :filtro_permanente, -> () {
      joins(:departamento).where('sip_departamento.id_pais' => 170) 
    }

  end
end

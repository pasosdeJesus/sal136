require 'mr519_gen/concerns/models/usuario'

class Usuario < ActiveRecord::Base
  include Mr519Gen::Concerns::Models::Usuario

  devise :registerable, :confirmable
  campofecha_localizado :confirmed_at
  campofecha_localizado :locked_at

  has_many :tabulae14, validate: true

  validates :email,
    length: {maximum: 255},
    presence: true,
    allow_blank: false
  validates :nombres,
    length: {maximum: 128},
    presence: true,
    allow_blank: false
  validates :apellidos,
    length: {maximum: 128},
    presence: true,
    allow_blank: false
  validates :institucion,
    length: {maximum: 500}
  validates :urlinstitucion,
    format: URI::regexp(%w[http https]),
    length: {maximum: 500},
    allow_blank: true

  def email_required?
    true
  end

  def presenta_nombre
    r = self.nusuario
    if self.nombres || self.apellidos
      r = (self.nombres ? self.nombres : '') + ' ' + 
        (self.apellidos ? self.apellidos : '')
    end
    r
  end

  def nombre
    r = self.nombres.to_s
    if self.apellidos
      r += " #{self.apellidos}"
    end
  end

  def presenta(atr)
    case atr.to_s
    when 'institucion'
      if urlinstitucion.to_s != ''
        "<a href='#{urlinstitucion.to_s}'>#{institucion || urlinstitucion.to_s}</a>".html_safe
      else
        institucion.to_s
      end
    else
      presenta_gen(atr)
    end
  end
  scope :filtro_nombres, lambda { |n|
    where("unaccent(nombres) ILIKE '%' || unaccent(?) || '%'", n)
  }

  scope :filtro_apellidos, lambda { |a|
    where("unaccent(apellidos) ILIKE '%' || unaccent(?) || '%'", a)
  }

end



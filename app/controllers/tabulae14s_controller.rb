class Tabulae14sController < ApplicationController
  before_action :set_tabulae14, only: %i[ show edit update destroy ]

  def clase
    "::Tabulae14"
  end

  # Genero del modelo (F - Femenino, M - Masculino)
  def genclase
    return 'M';
  end

  def atributos_index
    atributos_show
  end

  def new
    Sip::Municipio.conf_presenta_nombre_con_departamento = true

    if cannot? :new, clase.constantize
      render 'suscribirse_para_registrar', layout: 'application'
      return
    end
    eleccion_id = params[:eleccion_id] ? params[:eleccion_id].to_i : 
      Rails.configuration.x.eleccion 
    @registro = clase.constantize.new(tabulae14_params)
    if @registro.respond_to?('current_usuario=')
      @registro.current_usuario = current_usuario
    end
    if @registro.respond_to?(:fechacreacion)
      @registro.fechacreacion = DateTime.now.strftime('%Y-%m-%d')
    end

    nuevo_intermedio(@registro)

    if registrar_en_bitacora
      Sip::Bitacora.a(request.remote_ip, current_usuario.id,
                      request.url, params, @registro.class.to_s,
                      nil,  'iniciar', '')
    end

    @tabuladefoto = false
    if params[:mesa_id]
      m = Mesa.find(params[:mesa_id])
      @fotoe14 = Fotoe14.where(eleccion_id: eleccion_id, mesa_id: m.id).take
    else
      @fotoe14 = Fotoe14.para_tabular(
        eleccion_id, current_usuario)
      @tabuladefoto = true
    end

    puts "OJO eleccion_id=#{eleccion_id}, @tabuladefoto=#{@tabuladefoto}"
    if eleccion_id == 2  && @tabuladefoto
      render 'proximamente', layout: 'application'
      return
    end


    render layout: 'application'
  end

  def atributos_show
    r = [ 
      :id,
      :eleccion_id, 
      :usuario_id, 
      :municipio_id, 
      :zona, 
      :puesto, 
      :mesa, 
      :totalsufragantes, 
      :totalurna, 
      :totalincinerados, 
      :c1, :c2, :c3, :c4, :c5, :c6, 
      :c7, :c8, :c9, :c10, :c11, :c12, 
      :blanco, 
      :nulos, 
      :nomarcados, 
      :votosmesa, 
      :otrasconstancias, 
      :recuento_id, 
      :solicitadopor, 
      :enrepde, 
      :ccjurado1, 
      :ccjurado2, 
      :ccjurado3, 
      :ccjurado4, 
      :ccjurado5, 
      :ccjurado6,
      :fotoe14_id
    ]
    return r
  end

  def editar_intermedio(registro, usuario_actual_id)
    @tabuladefoto = registro.fotoe14 != nil
    if @tabuladefoto
      @fotoe14 = registro.fotoe14
    else
      mesa_id = nil
      if params[:mesa_id]
        mesa_id = params[:mesa_id]
      elsif registro.mesa && registro.puesto && registro.zona && 
        registro.municipio_id
        mun = Sip::Municipio.find(registro.municipio_id)
        if mun
          zona = Zona.where(municipio_id: mun.id, zona: registro.zona).take
          if zona
            puesto = Puesto.where(zona_id: zona.id, puesto: registro.puesto).take
            if puesto
              mesa = Mesa.where(puesto_id: puesto.id, mesa: registro.mesa).take
              if mesa
                mesa_id = mesa.id
              end
            end
          end
        end
      end
      if mesa_id
        mesa = Mesa.find(mesa_id)
        @fotoe14 = Fotoe14.where(eleccion_id: registro.eleccion_id, 
                                 mesa_id: mesa.id).take
      end
    end

    return true
  end



  def validaciones(registro)
      @validaciones_error = ''
    if registro.usuario_id.nil?
      registro.usuario_id = current_usuario.id
    elsif registro.usuario_id != current_usuario.id && 
      current_usuario.rol != ROLADMIN
      # No ocurre
      debugger
    end
    if registro.eleccion_id.nil?
      registro.eleccion_id = Rails.configuration.x.eleccion
    #elsif registro.eleccion_id != Rails.configuration.x.eleccion &&
    #  current_usuario.rol != ROLADMIN
      # No ocurre
    #  debugger
    end

    return true
  end

  def atributos_form
    Sip::Municipio.conf_presenta_nombre_con_departamento = true
    r = atributos_show - [:id]
    if !can?(:manage, :Tabulae14)
      r -= [:usuario_id]
    end
    return r
  end

  def filtra_ca(reg)
    reg = reg.habilitados
    return reg
  end

  def vistas_manejadas
    ['Tabulae14']
  end

  def nuevomismopuesto
    if params && params[:tabulae14_id] && 
        ::Tabulae14.where(id: params[:tabulae14_id].to_i).count == 1
      e = ::Tabulae14.find(params[:tabulae14_id].to_i)
      zona = Zona.where(municipio_id: e.municipio_id, zona: e.zona).take
      if !zona
        puts "Zona no encontrada #{e.zona} en municipio #{e.municipio_id}"
        return 
      end
      puesto = Puesto.where(zona_id: zona.id, puesto: e.puesto).take
      if !puesto
        puts "Puesto no encontrado #{e.puesto} en zona #{zona.id}"
        return
      end
      mesa = Mesa.where(puesto_id: puesto.id, mesa: e.mesa).take
      if !mesa
        puts "Mesa no encontrada #{e.mesa} en puesto #{puesto.id}"
        return 
      end
      redirect_to new_tabulae14_path(
        eleccion_id: e.eleccion_id,
        mesa_id: mesa.id, 
        tabulae14: {
          eleccion_id: e.eleccion_id,
          municipio_id: e.municipio_id,
          zona: e.zona,
          puesto: e.puesto,
          mesa: (::Tabulae14.where(
            usuario_id: current_usuario.id,
            municipio_id: e.municipio_id,
            zona: e.zona,
            puesto: e.puesto
          ).maximum(:mesa)) + 1
        })
    end
  end

  private
    def set_tabulae14
      @tabulae14 = @registro = Tabulae14.find(params[:id])
    end

    def lista_params
      atributos_form
    end

    # Only allow a list of trusted parameters through.
    def tabulae14_params
      params.fetch(:tabulae14, {}).permit(atributos_form)
    end
end

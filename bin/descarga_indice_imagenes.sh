#!/bin/sh
# Sugerido ejecutar  a diario (por ejemplo en /etc/daily.local)

. .env

if (test "$RAILS_ENV" = "development") then {
  bin/rails runner -e $RAILS_ENV guiones/descarga_indice_imagenes.rb
} elif (test "$RAILS_ENV" = "production") then {
  bin/rails runner -e $RAILS_ENV guiones/descarga_indice_imagenes.rb
} fi;

require "application_system_test_case"

class EleccionesTest < ApplicationSystemTestCase
  setup do
    @eleccion = eleccion(:one)
  end

  test "visiting the index" do
    visit elecciones_url
    assert_selector "h1", text: "Elecciones"
  end

  test "should create eleccion" do
    visit elecciones_url
    click_on "New eleccion"

    fill_in "Fechafin", with: @eleccion.fechafin
    fill_in "Fechaini", with: @eleccion.fechaini
    fill_in "Nombre", with: @eleccion.nombre
    fill_in "Numcandidatos", with: @eleccion.numcandidatos
    click_on "Create Eleccion"

    assert_text "Eleccion was successfully created"
    click_on "Back"
  end

  test "should update Eleccion" do
    visit eleccion_url(@eleccion)
    click_on "Edit this eleccion", match: :first

    fill_in "Fechafin", with: @eleccion.fechafin
    fill_in "Fechaini", with: @eleccion.fechaini
    fill_in "Nombre", with: @eleccion.nombre
    fill_in "Numcandidatos", with: @eleccion.numcandidatos
    click_on "Update Eleccion"

    assert_text "Eleccion was successfully updated"
    click_on "Back"
  end

  test "should destroy Eleccion" do
    visit eleccion_url(@eleccion)
    click_on "Destroy this eleccion", match: :first

    assert_text "Eleccion was successfully destroyed"
  end
end

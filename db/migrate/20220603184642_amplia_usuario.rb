class AmpliaUsuario < ActiveRecord::Migration[7.0]
  def up
    rename_column :usuario, :nombre, :nombres
    add_column :usuario, :apellidos, :string, limit: 128, 
      null: false, default: 'N'
    add_column :usuario, :institucion, :string, limit: 500
    add_column :usuario, :urlinstitucion, :string, limit: 1024
    add_column :usuario, :confirmation_token, :string
    add_column :usuario, :confirmed_at, :datetime
    add_column :usuario, :confirmation_sent_at, :datetime
    add_column :usuario, :unconfirmed_email, :string 
    add_column :usuario, :aceptappd, :string, limit: 5, 
      null: false, default: 't'
    add_column :usuario, :aceptacc, :string, limit: 5, 
      null: false, default: 't'
    add_index :usuario, :confirmation_token, unique: true
    execute("UPDATE usuario SET confirmed_at = NOW()")
    change_column :usuario, :nusuario, :string, limit: 255
  end

  def down
    change_column :usuario, :nusuario, :string, limit: 15
    remove_columns :usuario, :confirmation_token, :confirmed_at, 
            :confirmation_sent_at, :unconfirmed_email 
    remove_columns :usuario, :apellidos, :institucion, :urlinstitucion,
      :aceptacc, :aceptappd
    rename_column :usuario, :nombres, :nombre
  end
end

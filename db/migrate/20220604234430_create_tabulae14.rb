class CreateTabulae14 < ActiveRecord::Migration[7.0]
  def change
    create_table :tabulae14 do |t|
      t.integer :usuario_id, null: false
      t.integer :eleccion_id, null: false
      t.integer :municipio_id, null: false
      t.integer :zona, null: false
      t.integer :puesto, null: false
      t.integer :mesa, null: false
      t.integer :totalsufragantes, null: false
      t.integer :totalurna, null: false
      t.integer :totalincinerados, null: false
      t.integer :c1
      t.integer :c2
      t.integer :c3
      t.integer :c4
      t.integer :c5
      t.integer :c6
      t.integer :c7
      t.integer :c8
      t.integer :c9
      t.integer :c10
      t.integer :c11
      t.integer :c12
      t.integer :blanco, null: false
      t.integer :nulos, null: false
      t.integer :nomarcados, null: false
      t.integer :votosmesa, null: false
      t.string :otrasconstancias, limit: 1024
      t.boolean :recuento
      t.string :solicitadopor, limit: 255
      t.string :enrepde, limit: 255
      t.integer :ccjurado1, null: false
      t.integer :ccjurado2, null: false
      t.integer :ccjurado3
      t.integer :ccjurado4
      t.integer :ccjurado5
      t.integer :ccjurado6

      t.string :observaciones, limit: 1024
      t.timestamps
    end
    add_foreign_key :tabulae14, :usuario
    add_foreign_key :tabulae14, :eleccion
    add_foreign_key :tabulae14, :sip_municipio, column: :municipio_id
  end
end

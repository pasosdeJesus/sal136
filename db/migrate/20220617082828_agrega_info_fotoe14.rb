class AgregaInfoFotoe14 < ActiveRecord::Migration[7.0]
  def change
    add_column :fotoe14, :info, :string, limit: 100
    add_reference :fotoe14, :mesa, foreign_key: true
  end
end

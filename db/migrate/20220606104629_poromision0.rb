class Poromision0 < ActiveRecord::Migration[7.0]
  def change
    change_column_default :tabulae14, :zona, from: nil, to: 0
    change_column_default :tabulae14, :puesto, from: nil, to: 0
    change_column_default :tabulae14, :mesa, from: nil, to: 0
    change_column_default :tabulae14, :totalsufragantes, from: nil, to: 0
    change_column_default :tabulae14, :totalurna, from: nil, to: 0
    change_column_default :tabulae14, :totalincinerados, from: nil, to: 0
    change_column_default :tabulae14, :c1, from: nil, to: 0
    change_column_default :tabulae14, :c2, from: nil, to: 0
    change_column_default :tabulae14, :c3, from: nil, to: 0
    change_column_default :tabulae14, :c4, from: nil, to: 0
    change_column_default :tabulae14, :c5, from: nil, to: 0
    change_column_default :tabulae14, :c6, from: nil, to: 0
    change_column_default :tabulae14, :c7, from: nil, to: 0
    change_column_default :tabulae14, :c8, from: nil, to: 0
    change_column_default :tabulae14, :c9, from: nil, to: 0
    change_column_default :tabulae14, :c10, from: nil, to: 0
    change_column_default :tabulae14, :c11, from: nil, to: 0
    change_column_default :tabulae14, :c12, from: nil, to: 0
    change_column_default :tabulae14, :blanco, from: nil, to: 0
    change_column_default :tabulae14, :nulos, from: nil, to: 0
    change_column_default :tabulae14, :nomarcados, from: nil, to: 0
    change_column_default :tabulae14, :votosmesa, from: nil, to: 0
    change_column_default :tabulae14, :otrasconstancias, from: nil, to: ''
    change_column_default :tabulae14, :solicitadopor, from: nil, to: ''
    change_column_default :tabulae14, :enrepde, from: nil, to: ''
    change_column_default :tabulae14, :observaciones, from: nil, to: ''
  end
end

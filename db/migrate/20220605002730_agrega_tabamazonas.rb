class AgregaTabamazonas < ActiveRecord::Migration[7.0]
  def up
    execute <<-SQL
      INSERT INTO public.tabulae14 (id, usuario_id, eleccion_id, municipio_id,
        zona, puesto, mesa, totalsufragantes, totalurna, totalincinerados, 
        c1, c2, c3, c4, c5, c6, c7, c8,
        blanco, nulos, nomarcados, votosmesa,
        otrasconstancias, recuento, solicitadopor, enrepde,
        ccjurado1, ccjurado2, ccjurado3, ccjurado4, ccjurado5, ccjurado6,
        created_at, updated_at)
      VALUES (1, 1, 1, 703, 
        0, 0, 1, 208, 207, 0, 
        5, 1, 33, 0, 0, 164, 0, 0,
        0, 4, 0, 207,
        NULL, 'F', NULL, NULL, 
        6566338, 6566486, 97446605, 15878070, 41058411, 1097309120,
        '2022-06-04', '2022-06-04'
        );

      SELECT setval('public.tabulae14_id_seq', 100000000);
    SQL
  end
  def down
    execute <<-SQL
      DELETE FROM tabulae14 WHERE id>=1 AND id<=1;
    SQL
  end
end

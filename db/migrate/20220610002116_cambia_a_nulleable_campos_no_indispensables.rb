class CambiaANulleableCamposNoIndispensables < ActiveRecord::Migration[7.0]
  def change
    change_column_null :tabulae14, :blanco, from: false, to: true
    change_column_null :tabulae14, :nulos, from: false, to: true
    change_column_null :tabulae14, :nomarcados, from: false, to: true
    change_column_null :tabulae14, :votosmesa, from: false, to: true
  end
end

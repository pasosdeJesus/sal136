class DeshabilitaDepExterior < ActiveRecord::Migration[7.0]
  def up
    execute <<-SQL
      UPDATE sip_departamento SET fechadeshabilitacion='2022-06-16' 
        WHERE nombre='EXTERIOR';
    SQL
  end
  def down
    execute <<-SQL
      UPDATE sip_departamento SET fechadeshabilitacion=NULL
        WHERE nombre='EXTERIOR';
    SQL
  end
end

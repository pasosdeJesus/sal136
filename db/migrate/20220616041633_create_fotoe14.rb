class CreateFotoe14 < ActiveRecord::Migration[7.0]
  def change
    create_table :fotoe14 do |t|
      t.integer :id_externo, null: false
      t.string :url, null: false

      t.timestamps
    end

    add_reference :tabulae14, :fotoe14, foreign_key: true
    add_reference :fotoe14, :eleccion, foreign_key: true
  end
end

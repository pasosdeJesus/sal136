class VistaPromediosTabulacion < ActiveRecord::Migration[7.0]
  # Estas vistas tiene promedio y desviación descartando usuarios deshabilitados
  def up
    execute <<-SQL
      CREATE VIEW prome14 AS (
        SELECT t.eleccion_id, 
        t.municipio_id, 
        z.id AS zona_id, 
        p.id AS puesto_id, 
        m.id AS mesa_id,
        AVG(c1) AS c1,
        AVG(c2) AS c2,
        AVG(c3) AS c3,
        AVG(c4) AS c4,
        AVG(c5) AS c5,
        AVG(c6) AS c6,
        AVG(c7) AS c7,
        AVG(c8) AS c8,
        AVG(c9) AS c9,
        AVG(c10) AS c10,
        AVG(c11) AS c11,
        AVG(c12) AS c12,
        AVG(blanco) AS blanco,
        AVG(nomarcados) AS nomarcados,
        AVG(nulos) AS nulos
        FROM tabulae14 AS t
        JOIN eleccion AS e ON t.eleccion_id=e.id
        JOIN zona AS z ON e.geoelectoral_id=z.geoelectoral_id AND 
          z.municipio_id=t.municipio_id AND z.zona=t.zona
        JOIN puesto AS p ON p.zona_id=z.id AND p.puesto=t.puesto
        JOIN mesa AS m ON m.puesto_id=p.id AND m.mesa=t.mesa
        WHERE usuario_id IN (SELECT id FROM usuario 
          WHERE fechadeshabilitacion IS NULL)
        GROUP BY 1, 2, 3, 4, 5
      );
      CREATE VIEW desve14 AS (
        SELECT t.eleccion_id, 
        t.municipio_id,
        z.id AS zona_id, 
        p.id AS puesto_id, 
        m.id AS mesa_id,
        STDDEV_POP(c1) AS c1,
        STDDEV_POP(c2) AS c2,
        STDDEV_POP(c3) AS c3,
        STDDEV_POP(c4) AS c4,
        STDDEV_POP(c5) AS c5,
        STDDEV_POP(c6) AS c6,
        STDDEV_POP(c7) AS c7,
        STDDEV_POP(c8) AS c8,
        STDDEV_POP(c9) AS c9,
        STDDEV_POP(c10) AS c10,
        STDDEV_POP(c11) AS c11,
        STDDEV_POP(c12) AS c12,
        STDDEV_POP(blanco) AS blanco,
        STDDEV_POP(nomarcados) AS nomarcados,
        STDDEV_POP(nulos) AS nulos
        FROM tabulae14 AS t
        JOIN eleccion AS e ON t.eleccion_id=e.id
        JOIN zona AS z ON e.geoelectoral_id=z.geoelectoral_id AND 
          z.municipio_id=t.municipio_id AND z.zona=t.zona
        JOIN puesto AS p ON p.zona_id=z.id AND p.puesto=t.puesto
        JOIN mesa AS m ON m.puesto_id=p.id AND m.mesa=t.mesa
        WHERE usuario_id IN (SELECT id FROM usuario 
          WHERE fechadeshabilitacion IS NULL)
          GROUP BY 1, 2, 3, 4, 5
      );
    SQL
  end

  def down
    execute <<-SQL
      DROP VIEW prome14;
      DROP VIEW desve14;
    SQL
  end
end
